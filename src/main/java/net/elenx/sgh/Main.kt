package net.elenx.sgh

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileInputStream
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

val today: LocalDate = LocalDate.now()

const val header = "BEGIN:VCALENDAR\n" +
    "VERSION:2.0\n" +
    "BEGIN:VTIMEZONE\n" +
    "TZID:Europe/Warsaw\n" +
    "X-LIC-LOCATION:Europe/Warsaw\n" +
    "BEGIN:DAYLIGHT\n" +
    "TZOFFSETFROM:+0100\n" +
    "TZOFFSETTO:+0200\n" +
    "TZNAME:CEST\n" +
    "DTSTART:19700329T020000\n" +
    "RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\n" +
    "END:DAYLIGHT\n" +
    "BEGIN:STANDARD\n" +
    "TZOFFSETFROM:+0200\n" +
    "TZOFFSETTO:+0100\n" +
    "TZNAME:CET\n" +
    "DTSTART:19701025T030000\n" +
    "RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\n" +
    "END:STANDARD\n" +
    "END:VTIMEZONE\n"

const val footer = "END:VCALENDAR"

fun main(args: Array<String>)
{
    val file = File("/home/elentirald/Downloads/studia-magisterskie-harmonogram-zajec-sobotnio-niedzielne-zima.xlsx")
    val fileInputStream = FileInputStream(file)
    val workbook = XSSFWorkbook(fileInputStream)
    val sheet = workbook.getSheetAt(0)

    val lectures = sheet
        .asSequence()
        .filter { it.rowNum > 2 }
        .map { rowToLecture(it) }
        .filter { it.dates.isNotEmpty() }
        .filter { lecture -> lecture.dates.any { it.isAfter(today) } }

        .filter { isChosen(it) }

        .onEach { println(it) }
        .map { lectureToEvents(it) }
        .reduce { first, second -> first + second }

    val iCalContent = header + lectures + footer

    val iCalFile = File("/home/elentirald/Downloads/sgh.ical")

    if(iCalFile.exists()) iCalFile.delete()

    iCalFile.createNewFile()

    iCalFile.writeText(iCalContent)
}

data class Lecture(
    val name: String,
    val lecturer: String,
    val kind: String,
    val dayOfWeek: String,
    val startTime: LocalTime,
    val endTime: LocalTime,
    val group: Int,
    val place: String,
    val dates: List<LocalDate>
)

private fun rowToLecture(row: Row): Lecture
{
    val cells = (0..11).map { row.getCell(it) }.map { it.toString() }

    return Lecture(
        cells[3],
        cells[10],
        cells[4],
        cells[7],
        clockOf(cells[8]),
        clockOf(cells[9]),
        cells[5].toFloat().toInt(),
        cells[6],
        cells[11].split(';').filter { it != "" }.map { LocalDate.parse(it) }
    )
}

private fun clockOf(time: String): LocalTime
{
    val split = time.split(':')

    return LocalTime.of(split[0].toInt(), split[1].toInt())
}

private fun createEvent(dateTimeString: String, durationInMinutes: String, summary: String, description: String, location: String) =
    "BEGIN:VEVENT\n" +
    "UID:${UUID.randomUUID()}\n" +
    "DTSTAMP:${dateTimeString}\n" +
    "SUMMARY:${summary}\n" +
    "DTSTART;TZID=Europe/Warsaw:${dateTimeString}\n" +
    "DURATION:PT${durationInMinutes}M\n" +
    "DESCRIPTION:${description}\n" +
    "LOCATION:${location}\n" +
    "TRANSP:TRANSPARENT\n" +
    "END:VEVENT\n"

private fun lectureToEvents(lecture: Lecture) = lecture
    .dates
    .map { lectureToEvent(lecture, it) }
    .reduce { first, second -> first + second }

private fun lectureToEvent(lecture: Lecture, meetingDate: LocalDate): String
{
    val day = meetingDate.dayOfMonth.toString()
    val month = meetingDate.month.value.toString()
    val year = meetingDate.year.toString()

    val duration = lecture.endTime.minusHours(lecture.startTime.hour.toLong()).minusMinutes(lecture.startTime.minute.toLong())
    val durationInMinutes = duration.hour * 60 + duration.minute

    val localDateTime = LocalDateTime.of(meetingDate, lecture.startTime)
    val localDateTimeString = localDateTime
        .format(DateTimeFormatter.ofPattern("yyyyMMdd:HHmmss"))
        .replace(':', 'T') + 'Z'

    return createEvent(
        localDateTimeString,
        durationInMinutes.toString(),
        "[${lecture.kind[0]}] " + lecture.name,
        "Prowadzący: ${lecture.lecturer} \r\n Grupa: ${lecture.group}",
        lecture.place)
}

private fun matchesBlanks(lecture: Lecture): Boolean  = when(lecture.dayOfWeek)
{
//    "Poniedziałek" -> lecture.startTime.asHour() > 1850 && false
//    "Wtorek" -> lecture.startTime.asHour() > 1330
//    "Środa" -> lecture.startTime.asHour() > 1415
//    "Czwartek" -> false
//    "Piątek" -> lecture.startTime.asHour() > 1950 || true
    else -> false
}

private fun isChosen(lecture: Lecture): Boolean
{
    return true
}